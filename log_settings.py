import logging
import logging.config


log_config = {
    "version": 1,
    "formatters": {
        "stream_formatter": {
            "format": "%(asctime)s - %(levelname)s - %(message)s",
            'datefmt': "%d-%m %H:%M",
        },
        "file_formatter": {
            "format": "%(asctime)s - %(name)s - %(levelname)s - %(message)s",
            'datefmt': "%d-%m-%Y %H:%M",
        },
    },
    "handlers": {
        "stream_handler": {
            "class": "logging.StreamHandler",
            "formatter": "stream_formatter",
            "level": "INFO",
        },
        "file_handler": {
            "class": "logging.FileHandler",
            "formatter": "file_formatter",
            "filename": "bot.log",
            "encoding": "UTF-8",
            "level": "DEBUG",
        },
    },
    "loggers": {
        "bot": {
            "handlers": ["stream_handler", "file_handler"],
            "level": "DEBUG",
        },
    },
}
