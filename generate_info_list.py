from io import BytesIO

from PIL import Image, ImageFont, ImageDraw

TEMPLATE_PATH = r'files/1221.png'

FONT_PATH = r'files/OpenSans-Regular.ttf'
FONT_SIZE = 35
FONT_COLOR = (0, 0, 0, 255)

NAME_OFFSET = (1050, 270)
NUMBER_OFFSET = (1050, 350)
EMAIL_OFFSET = (1050, 430)


def generate_info_list(name, email, number):
    base = Image.open(TEMPLATE_PATH).convert('RGBA')
    font = ImageFont.truetype(FONT_PATH, FONT_SIZE)

    draw = ImageDraw.Draw(base)
    draw.text(NAME_OFFSET, name, font=font, fill=FONT_COLOR)
    draw.text(NUMBER_OFFSET, number, font=font, fill=FONT_COLOR)
    draw.text(EMAIL_OFFSET, email, font=font, fill=FONT_COLOR)

    # with open('files/ticket-probe.png', 'wb') as f:
    #     base.save(f, 'png')

    temp_file = BytesIO()
    base.save(temp_file, 'png')
    temp_file.seek(0)
    return temp_file


# generate_ticket(name='Ваня', email='email@email.com', number='+7(954)2458823')