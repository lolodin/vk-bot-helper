"""
Handler - функция, которая принимает на вход текст (текст входящего сообщения) и context (dict),
а возвращает bool: True если шаг пройден, False если данные введены неправильно.
"""
import re
from generate_info_list import generate_info_list

re_name = re.compile(r'^[\w\-\s]{3,30}$')
re_number = re.compile(r'\b\+?[7,8](\s*\d{3}\s*\d{3}\s*\d{2}\s*\d{2})\b')
re_email = re.compile(r'\b[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+\b')


def handle_name(text, context):
    match = re.match(re_name, text)
    if match:
        context['name'] = text
        return True
    else:
        return False


def handle_number(text, context):
    matches = re.findall(re_number, text)
    if len(matches) > 0:
        context['number'] = matches[0]
        return True
    else:
        return False


def handle_email(text, context):
    matches = re.findall(re_email, text)
    if len(matches) > 0:
        context['email'] = matches[0]
        return True
    else:
        return False


def generate_ticket_handler(text, context):
    return generate_info_list(name=context['name'], email=context['email'], number=context['number'])